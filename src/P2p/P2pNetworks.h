// Copyright (c) 2011-2016 The Cryptonote developers
// Distributed under the MIT/X11 software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#pragma once

namespace CryptoNote
{
  const static boost::uuids::uuid CRYPTONOTE_NETWORK = { { 0x40, 0xb2, 0xf9, 0x5f, 0x5a, 0x4f, 0x4a, 0x4b, 0xc4, 0x4c, 0x12, 0x13, 0x1c, 0x1f, 0xf1, 0x11 } };
}
